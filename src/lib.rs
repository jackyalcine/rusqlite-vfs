use std::io::{BufReader, BufWriter};
use std::{marker::Send, sync::Arc};

pub struct Bridge<H: vfs::FileSystem, F: Fn() -> H + Send + Sync> {
    pub fs_func: Arc<Box<F>>,
}

impl<H: vfs::FileSystem, F: Fn() -> H + Send + Sync> Bridge<H, F> {
    pub fn register(&self, vfs_name: &str) -> rusqlite::Result<()> {
        rusqlite::vfs::register(
            vfs_name,
            std::sync::Arc::new(Bridge {
                fs_func: Arc::clone(&self.fs_func),
            }),
            false,
        )
    }
}

#[derive(Clone)]
pub struct Item {
    path: vfs::VfsPath,
    lock: rusqlite::vfs::LockState,
    flags: rusqlite::vfs::OpenOptions,
}

fn from_vfs_error_to_io_err<R>(vfs_error: vfs::VfsResult<R>) -> std::io::Result<R> {
    vfs_error.map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e.to_string()))
}

impl rusqlite::vfs::Resource for Item {
    fn read_bytes(&mut self, buffer: &mut [u8], offset: usize) -> std::io::Result<()> {
        use std::io::{BufRead, Read};
        let file = from_vfs_error_to_io_err(self.path.open_file())?;
        let mut reader = BufReader::new(file);
        reader.seek_relative(offset as i64)?;
        reader.read(buffer).and(Ok(()))
    }

    // This is extremely cumbersome and requires the majority of the file to be read.
    fn write_bytes(&mut self, buffer: &[u8], offset: usize) -> std::io::Result<()> {
        use std::io::{Seek, Write};
        let mut file = from_vfs_error_to_io_err(self.path.update_file())?;
        let new_pos = file.seek(std::io::SeekFrom::Start(offset as u64))?;
        file.write_all(buffer)
    }

    fn file_size(&self) -> std::io::Result<usize> {
        from_vfs_error_to_io_err(self.path.metadata()).map(|m| m.len as usize)
    }

    fn has_moved(&self) -> std::io::Result<bool> {
        from_vfs_error_to_io_err(self.path.exists()).map(|b| !b)
    }
    fn close(&mut self) -> std::io::Result<()> {
        Ok(())
    }

    fn lock_state(&self) -> std::io::Result<rusqlite::vfs::LockState> {
        Ok(self.lock)
    }

    fn set_lock_state(&mut self, new_lock_state: rusqlite::vfs::LockState) -> std::io::Result<()> {
        self.lock = new_lock_state;
        Ok(())
    }
}

impl<H: vfs::FileSystem + Send, F: Fn() -> H + Send + std::marker::Sync> rusqlite::vfs::Filesystem
    for Bridge<H, F>
{
    type Handle = Item;

    fn full_pathname<'a>(&self, pathname: &'a str) -> std::io::Result<std::borrow::Cow<'a, str>> {
        Ok(std::borrow::Cow::Borrowed(pathname))
    }

    fn maximum_file_pathname_size() -> usize {
        1024
    }

    fn sector_size() -> usize {
        512
    }

    fn exists(&self, path: &str) -> std::io::Result<bool> {
        let root_vpath: vfs::VfsPath = (self.fs_func)().into();
        let vpath = from_vfs_error_to_io_err(root_vpath.join(&path))?;
        from_vfs_error_to_io_err(vpath.exists())
    }

    fn access(&self, path: &str, level: rusqlite::vfs::FileAccess) -> std::io::Result<bool> {
        let root_vpath: vfs::VfsPath = (self.fs_func)().into();
        let vpath = from_vfs_error_to_io_err(root_vpath.join(&path))?;
        match level {
            rusqlite::vfs::FileAccess::Readonly => Ok(false),
            rusqlite::vfs::FileAccess::Writable => Ok(true),
            rusqlite::vfs::FileAccess::ReadWrite => Ok(true),
        }
    }

    fn random_bytes(&self, buffer: &mut [i8]) {
        todo!()
    }

    fn sleep(&self, duration: std::time::Duration) -> std::time::Duration {
        todo!()
    }

    fn open(
        &self,
        path: String,
        flags: rusqlite::vfs::OpenOptions,
    ) -> std::io::Result<Self::Handle> {
        let root_vpath: vfs::VfsPath = (self.fs_func)().into();
        let vpath = from_vfs_error_to_io_err(root_vpath.join(&path))?;

        if flags.access.should_create() {
            let mut new_file = from_vfs_error_to_io_err(vpath.clone().create_file())?;
            new_file.flush()?;
        }

        Ok(Self::Handle {
            path: vpath,
            flags,
            lock: Default::default(),
        })
    }

    fn delete(&self, path: String) -> std::io::Result<()> {
        let root_vpath: vfs::VfsPath = (self.fs_func)().into();
        let vpath = from_vfs_error_to_io_err(root_vpath.join(&path))?;
        from_vfs_error_to_io_err(vpath.remove_file())
    }

    fn temporary_filename(&self) -> std::io::Result<String> {
        todo!()
    }
}

pub fn bridge<H: vfs::FileSystem, F: Fn() -> H + Send + Sync>(f: F) -> Bridge<H, F> {
    Bridge {
        fs_func: Arc::new(Box::new(f)),
    }
}
