use rusqlite::OpenFlags;
use std::sync::Arc;

pub struct PhysicalHooks(pub Arc<vfs::PhysicalFS>);

fn main() -> anyhow::Result<()> {
    let dir = std::env::current_dir()?;
    let physical_vfs = rusqlite_vfs::bridge(|| vfs::PhysicalFS::new(dir.clone()));
    physical_vfs.register("test-vfs")?;

    let conn_result = rusqlite::Connection::open_with_flags_and_vfs(
        "file:example-physical.sqlite",
        OpenFlags::SQLITE_OPEN_READ_WRITE | OpenFlags::SQLITE_OPEN_CREATE,
        "test-vfs",
    );

    assert_eq!(conn_result.as_ref().err(), None, "connection was opened");

    let conn = conn_result.unwrap();

    assert_eq!(
        conn.execute(
            r#"
                    CREATE TABLE IF NOT EXISTS person (
                        id      INTEGER PRIMARY KEY,
                        name    TEXT NOT NULL,
                        data    BLOB
                    );
                "#,
            []
        )
        .err(),
        // .as_ref()
        // .and_then(|e| e.sqlite_error())
        // .map(|e| unsafe { CStr::from_ptr(sqlite3_errstr(e.extended_code)) }),
        None,
        "creates a table"
    );
    for n in 0..10 {
        assert_eq!(
            conn.execute(
                "INSERT INTO person (name, data) VALUES (?1, ?2)",
                rusqlite::params!["Frederick Douglass".to_string(), Some(Vec::default())],
            )
            .err(),
            None,
            "inserting records"
        );
    }

    let stmt_result = conn.prepare("SELECT id, name, data FROM person");
    assert_eq!(stmt_result.as_ref().err(), None, "can build statements");
    let mut stmt = stmt_result.unwrap();

    let person_iter_result = stmt.query_map(rusqlite::params![], |row| row.get::<usize, u8>(0));
    assert_eq!(person_iter_result.as_ref().err(), None, "can get rows out");
    let person_iter = person_iter_result.unwrap();

    for person in person_iter {
        println!("Found person {:?}", person.unwrap());
    }

    assert_eq!(
        conn.execute(
            "ALTER TABLE person ADD COLUMN age INTEGER NOT NULL DEFAULT 18;",
            rusqlite::params![],
        )
        .err(),
        None,
        "altering a table"
    );

    Ok(())
}
